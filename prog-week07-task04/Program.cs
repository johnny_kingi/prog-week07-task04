﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week07_task04
{
    class Program
    {
        static void Main(string[] args)
        {
            task4();
        }
        static void task4 ()
        {
            var number1 = 12.22;
            var number2 = 3.33;

            Console.WriteLine(number1 + number2);
            Console.WriteLine(number1 - number2);
            Console.WriteLine(number1 / number2);
            Console.WriteLine(number1 * number2);
        }
    }
}
